
var app;
var storage = localforage;


function showError(msg) {
	var $t = $('#updateError');
	$t.text(msg).show();
	setTimeout(function() {
		$t.hide();
	}, 5000);
}

function updateWait() {
	$('#updateStatus').removeClass('ui-icon-check ui-icon-forbidden ui-icon-refresh wait ok error').addClass('ui-icon-refresh wait').show();
}

function updateOk() {
	$('#updateStatus').removeClass('ui-icon-check ui-icon-forbidden ui-icon-refresh wait ok error').addClass('ui-icon-check ok').show();
	setTimeout(function(){
		$('#updateStatus').hide();	
	}, 1000);
}

function updateError() {
	$('#updateStatus').removeClass('ui-icon-check ui-icon-forbidden ui-icon-refresh wait ok error').addClass('ui-icon-forbidden error').show();
}



RTGA = function() {
	var self = this;
	//var debug = false;
	var debug = true;
	const MAXQUALITY = 3;
	var remoteGameUpdater;
	
	GameUpdater = function() {
		var self = this;
		var shouldStop = false;
		var lastUpdate = new Date();
		var requestedUpdate = lastUpdate;
		const delay = 1000;
		
		self.update = function() {
			requestedUpdate = new Date();
			updateGameState();
			updateWait();
		}
		
		self.start = function() {
			if (shouldStop) {
				return;
			}
			if (requestedUpdate > lastUpdate) {
				var currentUpdate = requestedUpdate;
				var res = putGame(app.game);
				res.then(function() {
					lastUpdate = currentUpdate;
					updateOk();
					setTimeout(self.start, delay);
				});
				res.catch(function(){
					lastUpdate = currentUpdate;
					updateError();
					setTimeout(self.start, delay);
				});
			} else {
				setTimeout(self.start, delay);
			}
			
		};
		
		self.stop = function() {
			shouldStop = true;
		}
		
		return self;
	};

	
	function initCreatorId() {
		app.creatorId = localStorage.getItem('creatorId');
		log('creatorId = ' + app.creatorId);
		if (app.creatorId == null || app.creatorId == '') {
			app.creatorId = guid();
			localStorage.setItem('creatorId', app.creatorId);
		}
	}
	
	function initLastGame() {
		log('start initLastGame');
		var lastGameId = localStorage.getItem('lastGameId');
		log('lastGameId = ' + lastGameId);
		if (lastGameId != null && lastGameId != '') {
			return activateGame(lastGameId);
		}
		return new Promise(function(){});
	}
	
	function initLastTeam() {
		log('start initLastTeam');
		var lastTeamId = localStorage.getItem('lastTeamId');
		log('lastTeamId = ' + lastTeamId);
		if (lastTeamId != null && lastTeamId != '') {
			return activateTeam(lastTeamId);
		}
		return new Promise(function(){});
	}

	function initializeApp() {
		app = {
			game: {
				state: {
					events: [],
					segments: [],
					segmentDefaults: {}
				},
				id: null,
				name: '',
				home: '',
				visitor: ''
			},
			team: {
				id: null,
				name: ''
			}
		};
		initCreatorId();
		initLastTeam().then(initLastGame);
	}
	
	function initEvents() {
		$('#btnLeftPanel').click(onUpdateLeftPanel);
		$('#btnGameCreate').click(onCreateNewGame);
		$('#btnGameStart').click(onStartGame);
		$('#btnGameEdit').click(onEditGame);
		$('#btnGameOverConfirm').click(onGameOver);
		$('#btnUpdateEvents').click(onUpdateEvents);
		$('#btnTeamSave').click(onTeamSave);
		
		const touch = matchMedia('(hover: none)').matches;
		var startEvent = touch ? 'touchstart' : 'mousedown';
		var endEvent = touch ? 'touchend' : 'mouseup';

		$('.tagbutton').on(startEvent, tagStart);
		$('.tagbutton').on(endEvent, tagEnd);
		
		$(document).on('swipeleft', '.scoreBox', swipeLeftScore);
		$('.scoreBox').click(swipeRightScore);
		$(document).on('swiperight', '.scoreBox', swipeRightScore);
		
		$("#dlgSegmentDetails").popup();
		$('#btnSegmentDetailsSave').click(onSaveSegmentDetails);
		$('#btnSegmentDefaultsSave').click(onSaveSegmentDefaults);
		$('#btnSegmentDefaultsClear').click(onClearSegmentDefaults);

		$('.tabBtn').click(onTabChange).get(0).click();
		
		$('.base').click(onToggleBase);
	}

	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
	function log() {
		if (debug) {
			var args = [];
			for (var i=0; i < arguments.length; i++) {
				if ("object" == typeof arguments[i]) {
					args.push(JSON.stringify(arguments[i]));
				} else {
					args.push(arguments[i]);					
				}
			}
			console.log(args.join(' '));
		}
	}

	function updateTeamList() {
		log('updateTeamList: app =', app);
		return $.ajax({
			url: "teams.json",
			type: "GET",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (data, _textStatus, _jqXHR) {
				$lv = $('#listTeams').empty();
				$.each(data, function() {
					var logo = '<span class="logo" style="background:url(' + this.logo + ')"></span>';
					var selected = '';
					if (app.team.id === this.id) {
						//selected = '<i class="ui-btn ui-shadow ui-corner-all ui-icon-check ui-btn-icon-notext">Active</i>';
						selected = ' class="selected"';
					}
					$lv.append('<li teamId="' + this.id + '"' + selected + '>' + logo + this.name + '</li>');
				});
				$lv.listview('refresh');
				$lv.find('[teamId]').click(function(e){
					activateTeam($(e.currentTarget).attr('teamId'));
				});
			}
		});
	}
	
	async function getGames() {
		var teamId = app.team.id;
		var games = [];
		const keys = await storage.keys();
		for (var i=0; i < keys.length; i++) {
			var key = keys[i];
			console.log(key);
			if (key.split(':')[0] == teamId) {
				const value = await storage.getItem(key);
				console.log(value);
				games.push(JSON.parse(value));
			}
		}
		return games;
	}
	
	async function getGame(gameId) {
		var teamId = app.team.id;
		var expectedKey = teamId + ':' + gameId;
		log('getGame: ' + expectedKey);
		const value = await storage.getItem(expectedKey);
		return JSON.parse(value);
	}
	
	async function updateGameList() {
		log('updateGameList: app =', app);
		var games = await getGames();
		console.log('all games = ' + JSON.stringify(games));
		$lv = $('#listGames').empty();
		for (var i=0; i < games.length; i++) {
			var g = games[i];
			var label = [];
			label.push(g.name);
			if (!g.started) {
				label.push('<span style="position:absolute;"><a href="#dlgGameStart" data-rel="popup" data-gameId="' + g.id + '" data-position-to="window" data-transition="pop" class="ui-btn-icon-right ui-icon-arrow-r"></a></span>');
			}
			var inprogress = g.completed == null || '' == g.completed;
			if (!inprogress) {
				label.push('  ' + g.finalHome + "-" + g.finalVisitor);
			}
			var selected = (app.game.id === g.id) ? 'selected' : ''; 
			var lclass = inprogress ? 'inprogress' : 'done';
			$lv.append('<li gameId="' + g.id + '" class="' + lclass + ' ' + selected + '">' + label.join('') + '</li>');
		}
		$lv.listview('refresh');
		$lv.find('[gameId]').click(function(e){
			activateGame($(e.currentTarget).attr('gameId'));
		});
	}
	
	function activateTeam(teamId) {
		log('activateTeam: app =', app, 'teamId =', teamId);
		return $.ajax({
			url: "teams.json",
			type: "GET",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			headers: {
				"X-CSRFToken": RTGA.csrf_token,
				"X-CREATORID": app.creatorId
			},
			success: function (data, _textStatus, _jqXHR) {
				log('teams.json returned');
				app.team = {};
				for (var i=0; i < data.length; i++) {
					if (data[i].id == teamId) {
						app.team = data[i];
						break;
					}
				}

				//console.log(JSON.stringify(app));
				$('#teamHeader').text(app.team.name);
				$('<img/>').css('margin', '0 0 0 10px').css('vertical-align', 'middle').attr('src', app.team.logo).appendTo($('#teamHeader'));

				localStorage.setItem('lastTeamId', app.team.id);
				updateTeamList();
				updateGameList();
				
				$('#teamName').val(app.team.name);
				$('#teamShortName').val(app.team.shortname);
				$('#teamPlayers').val(app.team.players.join('\n'));
				
				var $subjects = $('#segmentDefaultSubject').empty().val('');
				$.each(app.team.players, function(_i, v) {
					var opt = $('<option/>').text(v);
					opt.appendTo($subjects);
				});
				//$('#segmentSubject').val(segment.subject || '');
				$('#segmentDefaultSubject').selectmenu('refresh');
				log('teams.json return processed');
			}
		});
	}
	
	async function activateGame(gameId) {
		log('activateGame: app =', app, 'gameId =', gameId);
		const g = await getGame(gameId);
		if (g == null) {
			return;
		}
		app.game = g;
		log('found game ' + gameId + ' = ' + JSON.stringify(app));
		$('#gameHeader').text('RTGA: ' + app.game.name);
		$('<img/>').css('margin', '-6px 0 0 10px').css('position', 'absolute').attr('src', app.team.logo).appendTo($('#gameHeader'));
		$('#btnLeftPanel').click();

		localStorage.setItem('lastGameId', app.game.id);
		updateGameState();
		/*
		return $.ajax({
			url: "/rtga/game/" + gameId,
			type: "GET",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			headers: {
				"X-CSRFToken": RTGA.csrf_token,
				"X-CREATORID": app.creatorId
			},
			success: function (data, _textStatus, _jqXHR) {
				app.game = data;
				log(JSON.stringify(app));
				$('#gameHeader').text('RTGA: ' + app.game.name);
				$('<img/>').css('margin', '-6px 0 0 10px').css('position', 'absolute').attr('src', app.team.logo).appendTo($('#gameHeader'));
				$('#btnLeftPanel').click();

				localStorage.setItem('lastGameId', app.game.id);
				updateGameState();
			}
		});
		*/
	}
	
	function onCreateNewGame() {
		//app.game = {};
		app.game.id = null;
		app.game.name = $('#gameName').val();
		app.game.home = $('#gameHome').val();
		app.game.visitor = $('#gameVisitor').val();
		app.game.state = {
			events: [],
			segments: [],
			segmentDefaults: {}
		};
		updateGameEvents();
		app.game.state.events.push(getCurrentGameEvent('00:00:00'));
		
		pushState(updateGameList);
		
		log(JSON.stringify(app));
		$('#btnLeftPanel').click();
	}
	
	function onEditGame() {
		
		app.game.name = $('#gameEditName').val();
		app.game.home = $('#gameEditHome').val();
		app.game.visitor = $('#gameEditVisitor').val();
		pushState(updateGameList);
		
		log(JSON.stringify(app));
	}
	
	function putGame(game) {
		if (!game.id) {
			showError('Game must be started first!');
			return;
		}
		var key = app.team.id + ':' + game.id;
		log('putGame: ' + key + ', ' + game);
		return storage.setItem(key, JSON.stringify(game));
	}
	
	function pushState(onComplete) {
		if (!app.game.id) {
			app.game.id = new Date().valueOf();
		}
		var key = app.team.id + ':' + app.game.id;
		storage.setItem(key, JSON.stringify(app.game)).then(onComplete);
	}
	
	function onStartGame() {
		log('StartGame: app =', app);
		app.game.started = new Date().valueOf();
		updateGameState();
	}
	
	function onTeamSave() {
		var data = {
			team: {
				name: $('#teamName').val(),
				shortname: $('#teamShortName').val(),
				players: $('#teamPlayers').val()
			}
		};
		$.ajax({
			url: "/rtga/team/",
			type: "POST",
			data: JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			headers: {
				"X-CSRFToken": $(document.body).data('csrf-token'),
				"X-CREATORID": app.creatorId
			},
			success: function (data, _textStatus, _jqXHR) {
				log(JSON.stringify(data));
			}
		});
	}
	
	function tagStart() {
		if (!app.game.started) {
			$('#updateError').show().text('Start the game first!');
			return;
		}
		log('touch start');
		$(this).parent().addClass('tagregion-active');
		$(this).find('.label').text('Tagging');
		//$('#updateError').text('touch start').show();
		$(this).data('start', new Date());
	}
	
	function roundDateUpToNearestSecond(d) {
		if (d.getUTCMilliseconds() > 0) {
			d.setUTCSeconds(d.getUTCSeconds() + 1);
			d.setUTCMilliseconds(0);
		}
		return d;
	}
	
	function tagEnd() {
		$(this).parent().removeClass('tagregion-active');
		$(this).find('.label').text('Tap and Hold');
		var start = $(this).data('start');
		var end = roundDateUpToNearestSecond(new Date());
		var tagStart = timeToTag(start);
		var tagEnd = timeToTag(end);
		log('touch complete: ' + (end - start));
		log(tagStart + " -> " + tagEnd);
		//$('#updateError').text('touch complete: ' + (end - start));
		
		if (!app.game.state) {
			app.game.state = {};
		}
		if (!app.game.state.segments) {
			app.game.state.segments = [];
		}
		var segment = {
			from: tagStart,
			to: tagEnd
		};
		if (app.game.state.segmentDefaults) {
			segment = Object.assign({}, segment, app.game.state.segmentDefaults);
		}
		app.game.state.segments.push(segment);
		
		remoteGameUpdater.update();
		//updateGameState();
	}
	
	function timeToTag(time) {
		// convert start and end times to time delta in format hh:mm:ss 
		var d = new Date(new Date(time) - new Date(app.game.started));
		return d.toISOString().slice(11, 19);
	}
	
	function tagToTime(tag) {
		// convert start and end times to time delta in format hh:mm:ss 
		return Date.parse('01 Jan 1970 ' + tag + ' GMT');
	}

	function updateGameState() {
		log('UpdateGameState: app =', app);
		updateGameSegments();
		updateGameEvents();
		updateSegmentDefaults();
		
		$('#gameEditName').val(app.game.name);
		$('#gameEditHome').val(app.game.home);
		$('#gameEditVisitor').val(app.game.visitor);
		$('#gameOverName').val(app.game.name);
		$('#gameOverHome').val(app.game.home);
		$('#gameOverVisitor').val(app.game.visitor);
		
		$('#updateError').hide();
		//$('#updateError').text(app.creatorId).show();
		//$('#updateError').text(JSON.stringify(app)).show();
	}
	
	function qualityBullseye(quality) {
		var sb = [];
		var w = 30;
		var h = 30;
		var cx = w / 2;
		var cy = h / 2;
		var corer = 5 + (MAXQUALITY - quality) * 3 - 1;
		var opacity = (quality + 1) * (100.0 / (MAXQUALITY + 1));
		sb.push('<svg viewBox="0 0 ' + w + ' ' + h + '" width="' + w + '" height="' + h + '" class="starSegment" style="opacity:' + opacity + '%; margin-left:3px;">');
		// one red bullseye ring for each quality point
		for (var i=quality; i > 0; i--) {
			var r = corer + i * 3 - 1;
			sb.push('<circle cx="' + cx + '" cy="' + cy + '" r="' + r + '" fill="#fff" stroke="red" stroke-width="1.5" />');
		}
		// always one red dot in middle
		sb.push('<circle cx="' + cx + '" cy="' + cy + '" r="' + corer + '" fill="red" />');
		sb.push('</svg>');
		return sb.join("\n");
	}
	
	function updateGameSegments() {
		$lv = $('#listSegments').empty();
		var totalTime = 0;
		$.each(app.game.state.segments, function() {
			var label = [];
			label.push(this.from + " -> " + this.to);
			var quality = this.quality || 0;
			label.push(qualityBullseye(quality));
		
			var timeFrom = tagToTime(this.from);
			var timeTo = tagToTime(this.to);
			totalTime += (timeTo - timeFrom);
			$lv.append('<li>' + label.join('') + '</li>');
			$lv.find('li:last-child').data('segment', JSON.stringify(this));
		});
		$lv.listview('refresh');
		
		$lv.find('li').click(onClickSegment);
		$lv.find('.starSegment').click(onClickStarSegment);
		
		setTimeout(function(){
			$("#segments").scrollTop($("#segments")[0].scrollHeight);
		}, 100);
		
		try {
			var details = 'segments: ' + app.game.state.segments.length + ', ' + new Date(totalTime).toISOString().slice(11, 19);
			$('#segmentDetails').text(details);
		} catch(err) {
		}
	}
	
	function onClickStarSegment(e) {
		e.stopPropagation();
		var segment = JSON.parse($(this).parent('li').data('segment'));
		log('incr star quality: ' + JSON.stringify(segment));
		incrSegmentQuality(segment);
	}
	
	function onClickSegment(_e) {
		//log('onClickSegment');
		$('#dlgSegmentDetails').popup('open');
		
		var segment = JSON.parse($(this).data('segment'));
		$('#segmentFrom').val(segment.from);
		$('#segmentTo').val(segment.to);
		var $subjects = $('#segmentSubject').empty().val('');
		$.each(app.team.players, function(_i, v) {
			var opt = $('<option/>').text(v);
			opt.appendTo($subjects);
		});
		$('#segmentSubject').val(segment.subject || '');
		$('#segmentSubject').selectmenu('refresh');
		$('#segmentNote').val(segment.note || '');
		$('#segmentQuality').val(segment.quality || '0');
		$('#segmentQuality').selectmenu('refresh');
		$('#segmentAction').val(segment.action || 'hitting');
		$('#segmentAction').selectmenu('refresh');
	}
	
	function updateGameEvents() {
		if (app.game.state.events.length > 0) {
			var lastEvent = app.game.state.events[app.game.state.events.length - 1];
			$('#eventInning').text(lastEvent.inning);
			$('#eventHome').text(lastEvent.home);
			$('#eventVisitor').text(lastEvent.visitor);
			$('#eventOuts').text(lastEvent.outs);
			setBasesOccupiedState(lastEvent.bases);
		} else {
			$('#eventInning').text('Top 1');
			$('#eventHome').text('0');
			$('#eventVisitor').text('0');
			$('#eventOuts').text('0');
			$('.field .base').removeClass('occupied');
		}
	}
	
	function getCurrentGameEvent(eventAt) {
		return {
			inning: $('#eventInning').text(),
			home: $('#eventHome').text(),
			visitor: $('#eventVisitor').text(),
			outs: $('#eventOuts').text(),
			bases: getBasesOccupiedState(),
			at: eventAt || timeToTag(new Date())
		};
	}
	
	function onUpdateEvents() {
		var event = getCurrentGameEvent();
		app.game.state.events.push(event);
		
		var inningHalf = event.inning.slice(0,3).toLowerCase();
		log('setting default action: inningHalf =', inningHalf, 'Visitor =', (app.team.shortname === app.game.visitor), 'Home =', (app.team.shortname === app.game.home));
		if ("top" === inningHalf) {
			if (app.team.shortname === app.game.visitor) {
				$('#segmentDefaultAction').val('hitting');	
			} else if (app.team.shortname === app.game.home) {
				$('#segmentDefaultAction').val('pitching');
			}
		} else if ("bot" === inningHalf) {
			if (app.team.shortname === app.game.home) {
				$('#segmentDefaultAction').val('hitting');	
			} else if (app.team.shortname === app.game.visitor) {
				$('#segmentDefaultAction').val('pitching');
			}
		}
		$('#segmentDefaultAction').selectmenu('refresh');
		
		remoteGameUpdater.update();
	}
	
	function onClearSegmentDefaults() {
		app.game.state.segmentDefaults = {};
		updateSegmentDefaults();
	}
	
	function onSaveSegmentDefaults() {
		var segDefaults = {
			subject: $('#segmentDefaultSubject').val(),
			action: $('#segmentDefaultAction').val(),
		};
		app.game.state.segmentDefaults = segDefaults;
		
		updateSegmentDefaults();
	}
	
	function updateSegmentDefaults() {
		var segDefaults = app.game.state.segmentDefaults;
		if (segDefaults && segDefaults.action && segDefaults.subject) {
			var abbrev = segDefaults.action.slice(0,1).toUpperCase() + ': ' + segDefaults.subject;
			$('#btnSegmentDefaults').text(abbrev);
		} else {
			$('#btnSegmentDefaults').text('Segment Defaults');
		}
	}
	
	function saveSegment(segment) {
		var segments = app.game.state.segments;
		for (var i=0; i < segments.length; i++) {
			var s = segments[i];
			if (s.from === segment.from && s.to === segment.to) {
				s.quality = segment.quality;
				s.subject = segment.subject;
				s.action = segment.action;
				s.note = segment.note;
				break
			}
		}

		remoteGameUpdater.update();
	}
	
	function onSaveSegmentDetails() {
		//log('saveSegmentDetails');
		var segment = {
			from: $('#segmentFrom').val(),
			to: $('#segmentTo').val(),
			quality: $('#segmentQuality').val(),
			subject: $('#segmentSubject').val(),
			action: $('#segmentAction').val(),
			note: $('#segmentNote').val(),
		};
		saveSegment(segment);
	}
	
	function incrSegmentQuality(segment) {
		if (!segment.quality) {
			segment.quality = 0;
		}
		segment.quality++;
		if (segment.quality > MAXQUALITY) {
			segment.quality = 0;
		}
		saveSegment(segment);
	}
	
	function swipeLeftScore(_e) {
		incScore($(this), -1);
	}
	
	function swipeRightScore(_e) {
		incScore($(this), 1);
	}
	
	function incScore(el, delta) {
		var cur = el.text();
		var vals = el.data('vals').split(',');
		var curindex = vals.indexOf(cur);
		curindex += delta;
		if (curindex >= 0 && curindex < vals.length) {
			el.text(vals[curindex]);
		} else {
			el.text(vals[0]);
		}
	}
	
	function onUpdateLeftPanel() {
		updateTeamList().then(updateGameList);
	}
	
	function onGameOver() {
		if (app.game.id) {
			app.game.name = $('#gameOverName').val();
			app.game.home = $('#gameOverHome').val();
			app.game.visitor = $('#gameOverVisitor').val();
			app.game.comments = $('#gameOverComments').val();
			if (app.game.events && app.game.events.length > 0) {
				app.game.finalHome = app.game.events[app.game.events.length - 1].home;
				app.game.finalVisitor = app.game.events[app.game.events.length - 1].visitor;
			}
			app.game.completed = new Date().valueOf();
			console.log('GAME OVER');
			remoteGameUpdater.update();
			updateGameList();
			
			/*
			$.ajax({
				url: "/rtga/game/" + app.game.id + "/over",
				type: "POST",
				data: $('#gameOverForm').serialize(),
				headers: {
					"X-CSRFToken": RTGA.csrf_token,
					"X-CREATORID": app.creatorId
				},
				success: function(_data, _textStatus, _jqXHR) {
					updateGameList();
				},
				statusCode: {
					400: function() {
						showError('Not your game to finish!');
					}
				}
			});
			*/
		} else {
			showError('No Active Game Selected');
		}
	}

	function onTabChange(_e, ui) {
		//log('calling onTabChange');
		var newTab;
		if (!ui) {
			newTab = $(this).text();
		} else if (ui.tab) {
			newTab = ui.tab.text();
		} else if (ui.newTab) {
			newTab = ui.newTab.text();
		}
		//log(newTab);
		if ("Teams" === newTab) {
			$('#team-actions').show();
			$('#game-actions').hide();
		} else if ("Games" === newTab) {
			$('#team-actions').hide();
			$('#game-actions').show();
		}
	}
	
	function onToggleBase(_e) {
		var $base = $(this);
		if ($base.hasClass('occupied')) {
			$base.removeClass('occupied');
		} else {
			$base.addClass('occupied');
		}
	}
	
	function getBasesOccupiedState() {
		//return $('.field .base').map(function() { return $(this).hasClass('occupied'); }).get();
		return [
			$('.field .base1').hasClass('occupied'),
			$('.field .base2').hasClass('occupied'),
			$('.field .base3').hasClass('occupied')
		];
	}
	
	function setBasesOccupiedState(bases) {
		if (bases) {
			for (var i=0; i < bases.length; i++) {
				if (bases[i]) {
					$('.field .base' + (i+1)).addClass('occupied');
				} else {
					$('.field .base' + (i+1)).removeClass('occupied');
				}
			}
		}
	}
	
	self.init = function() {
		console.log('start init');
		self.csrf_token = $(document.body).data('csrf-token');
		try {
			//app = JSON.parse(localStorage.getItem('app'));
			if (app == null) {
				initializeApp();
			}
			//console.log('app = ' + JSON.stringify(app));
		} catch(e) {
			initializeApp();
		}
		initCreatorId();
		updateGameState();
		
		initEvents();
		
		remoteGameUpdater = new GameUpdater();
		remoteGameUpdater.start();
		console.log('end init');
	};
	
	self.csrf_token = 'UNDEFINED';
	
	return self;
}();

$(function(){
	RTGA.init();
});

